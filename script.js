const form = document.querySelector("#form");


form.addEventListener("submit", (e) => {
    e.preventDefault();
    // console.log(e);
    const emailInput = document.querySelector("#email");
    const errorMsg = document.querySelector("#error-msg")
    const matchEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (!matchEmail.test(emailInput)) {
        // Do something with valid email
        errorMsg.style.opacity = 1
        emailInput.style.borderColor = "hsl(354, 100%, 66%);"
        emailInput.style.outline = "hsl(354, 100%, 66%);"
        // errorMsg.style.display = "inline-block"

        console.log("The email address is valid!");
       
} else {
    setTimeout(() => {
        errorMsg.style.opacity = 0
        emailInput.style.borderColor = "hsl(223, 87%, 63%)"
        emailInput.style.outline = "hsl(223, 87%, 63%)" 
        console.log("The email address not valid !");
    }, 2000)
}
})